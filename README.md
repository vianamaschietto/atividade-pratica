# Questões teóricas

## 1. O que é Git?
É um sistema utilizado para o controle de versionamento do código fonte.

## 2. O que é a staging area?
É a área, no qual os arquivos modificados estão esperando para serem adicionadas ao próximo commit.

## 3. O que é o working directory?
São todos os arquivos que estão sendo trabalhados.

## 4. O que é um commit?
O commit é uma snapshot dos arquivos modificados na staging area, mantendo-o no repositório local.

## 5. O que é uma branch?
Branch é uma ramificação do código, o qual aponta para um commit, e tudo anterior a ele.

## 6. O que é o head no Git?
Head faz referência ao commit que você está vendo no momento ou branch.

## 7. O que é um merge?
Ele é um comando responsável por interagir, juntar, duas branches no git.

## 8. Explique os 4 estados de um arquivo no Git.
Os possíveis estados para um arquivo git são: untracked, arquivos que não estavam no último commit, unmodified, os arquivos que não foram modificados desde o último commit, modified, os modificados no último commit e, por fim, staged, arquivos prontos para comitar.

## 9. Explique o comando git init.
O comando init tem a funcionalidade de criar um repositório vazio ou reinicializar um já existente.

## 10. Explique o comando git add.
O comando add serve para adicionar novos arquivos a staging area.

## 11. Explique o comando git status.
O status tem a função de mostrar o status da branch, literalmente. Nesse sentido, ele mostra ao usuário a diferença entre o arquivo que está sendo trabalhado com o HEAD, entre outras coisas.

## 12. Explique o comando git commit.
O Commit server para salvar as mudanças feitas na staging area.

## 13. Explique o comando git log.
O comando log serve para os programadores olharem o histórico de commits feitos no repositório.

## 14. Explique o comando git checkout -b.
Esse comando serve para adicionarmos ramos às branches.

## 15. Explique o comando git reset e suas três opções.
O git reset é separado em três opções: soft, move o HEAD mantendo tanto o working directory quanto o staging area inalterados, mixed, move o HEAD, altera a staging area e mantém o working directory, por fim o hard ele altera o HEAD, para algum commit anterior, e junto dele o comando altera o directory e o staging.

## 16. Explique o comando git revert.
O revert tem a função de alterar o commit atual para algum commit anterior a ele. 

## 17. Explique o comando git clone.
O comando clone serve para clonar um repositório em um novo repositório.

## 18. Explique o comando git push.
O git push é utilizado para transferir commits de um repositório local para um repositório remoto.

## 19. Explique o comando git pull.
O comando busca e integra um repositório ou uma branch local.

## 20. Como ignorar o versionamento de arquivos no Git?
Para ignorar versionamentos no git, basta criar um arquivo chamado .gitignore. Assim, ao realizar um commit os arquivos dentro do .gitignore serão ignorados.

## 21. No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.
A separação das branches tem como objetivo organizar o fluxo do trabalho. O branch master serve como a versão final, após as modificações. O develop é um ramo do master utilizado para incluir alterações ao sistema. O ramo staging é criado a partir do develop, ele é usado para fazer testes nas alterações dos códigos feitos pelos desenvolvedores. É importante ressaltar que são feitas algumas rotinas de testes regressivas, onde após os testes o staging é mesclado com o master gerando uma nova versão do programa.

# Questões práticas

## 3. Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:
```js
const args = process.argv.slice(2);
console.log(parseInt(args[0]) + parseInt(args[1]));
```

O codigo tem a funcao de somar dois numeros que foram passados na linha de comando, para isso o programador salvou em uma constante args os valores. E importante notar que ele usou "argv" para armazenar os valores digitados no prompt.
Alem disso, para executar o codigo foi necessario fazer o download de node.js, apos isso basta digitar no prompt {node.js "nome do arquivo" valor1 valor2}. 

## 4. Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele. Que tipo de commit esse código deve ter de acordo ao conventional commit. Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. Por fim, faça um push desse commit.
Ao usar {git add .} voce adiciona todos untracked archives do working directory para a stage area, porem ao usar {git add "nome do arquivo"} voce estara passando apenas um arquivo.
E importante ressaltar que o commit, o qual adiciona novas funcionalidade(calculadora.js), usa-se o tipo feat e em casos de arquivo para decomentacao(README.md), usa-se o tipo docs.

## 5. Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança. 
```js
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const args = process.argv.slice(2);

soma();
```

O commit utilizado para essa situacao e o refactor, onde o codigo e modificado porem a funcao continua a mesma.

## 6. Depois disso, realizou um git add . e um commit com a mensagem: "Feature: added subtraction" faça como ele e descubra como executar o seu novo código. Nesse código, temos um pequeno erro, encontre-o e corrija para que a soma e divisão funcionem. Por fim, commit sua mudança. 
```js
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
    console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}
```

Os erros encontrados foram, dentro do console.log o nome da variavel eh args e nao arg; e as variaveis passadas na soma e sub deveriam ser args[1] e args[2], nao arg[0] e args[1].

## 7. Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.
Para criar uma branch basta usar o comando {git checkout -b "nome dessa branch"}, assim vc podera desenvolver o codigo sem problemas.

## 8. Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.
Para executar um merge entre duas branches, voce deve entrar na branch principal e executar o comando {main branch/ secundary branch}.

## 9. Faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!
Para fazer o revert bastou usar o comando {git revert HEAD}.

## 10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem. 
A maneira de executar o codigo eh {node calculadora valor1 operacao valor2};
A funcao eval permite a utilizacao o simbolo digitado pelo usuario como um operador.